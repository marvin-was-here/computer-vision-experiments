import threading
from threading_class_import import NewSocket
import sys

# server_ip = 10.19.48.109 # apl macbook
server_ip = "192.168.0.13" # home macbook

t1 = NewSocket(1, server_ip, 8000)
t2 = NewSocket(2, server_ip, 8001)
t3 = NewSocket(3, server_ip, 8002)
t4 = NewSocket(4, server_ip, 8004)


t1.run()
t2.run()
t3.run()
t4.run()

while True:
    print("[INPUT] Type \"image\" to request an image, type \"quit\" to exit")
    x = input()

    if x == "quit":
        print("[STATUS] Stopping Program")
        sys.exit()
    else:
        tA = threading.Thread(target=t1.request_image)
        tB = threading.Thread(target=t2.request_image)
        tC = threading.Thread(target=t3.request_image)
        tD = threading.Thread(target=t4.request_image)

        tA.start()
        tB.start()
        tC.start()
        tD.start()

        tA.join()
        tB.join()
        tC.join()
        tD.join()

        print("Threads A B C D finished")
