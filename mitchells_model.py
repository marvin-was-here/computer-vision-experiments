import cv2 as cv
from imutils.perspective import four_point_transform
from imutils import contours
import imutils
from torchvision import datasets

import numpy as np
import torch
import torchvision
import matplotlib.pyplot as plt
from time import time
from torchvision import datasets, transforms
from torch import nn, optim
from torch.autograd import Variable

# Map of digits lookup
DIGITS_LOOKUP = {
	(1, 1, 1, 0, 1, 1, 1): 0,
	(0, 0, 1, 0, 0, 1, 0): 1,
	(1, 0, 1, 1, 1, 1, 0): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,
	(1, 1, 0, 1, 0, 1, 1): 5,
	(1, 1, 0, 1, 1, 1, 1): 6,
	(1, 0, 1, 0, 0, 1, 0): 7,
	(1, 1, 1, 1, 1, 1, 1): 8,
	(1, 1, 1, 1, 0, 1, 1): 9
}

#     0
#   1   2
#     3
#   4   5
#     6

image = cv.imread("images/test_number_6_2.png", 0)
image = cv.resize(image, (28, 28))
# cv.imshow("img", image)
# cv.waitKey(0)




def process_image(image):
	image = imutils.resize(image, height=500)
	gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
	blurred = cv.GaussianBlur(gray, (5, 5), 0)
	edged = cv.Canny(blurred, 50, 200, 255)

	cnts = cv.findContours(edged.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	cnts = sorted(cnts, key=cv.contourArea, reverse=True)[2:5]
	displayCnt = None


	for c in cnts:
		peri = cv.arcLength(c, True)
		approx= cv.approxPolyDP(c, 0.02 * peri, True)


		if len(approx) == 4:
			displayCnt = approx
			break


	warped = four_point_transform(gray, displayCnt.reshape(4, 2))
	output = four_point_transform(image, displayCnt.reshape(4, 2))

	# cv.drawContours(output, [displayCnt], -1, (0, 255, 0), 2)

	warped = imutils.resize(warped, height=700)
	output = imutils.resize(output, height=700)

	# morpho
	thresh = cv.threshold(warped, 0, 255,
		cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
	kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (1, 5))
	thresh = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel)

	cv.imshow("image", thresh)
	cv.waitKey(0)


# process_image(image)

### Pytorch Processing

model = torch.load("my_mnist_model.pt")


model.eval()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model.to(device)

print("model loaded")

def predict_image(image):
		with torch.no_grad():
			image = torch.from_numpy(image)
			image = image.to(device)
			image = image.float().reshape(image.shape[0]*image.shape[1], -1).T
			prediction = model(image)

			ps = torch.exp(prediction)
			probab = list(ps.numpy()[0])
			print(probab)
			pred_label = probab.index(max(probab))
			pred_label += 1

			print("prediction is {}".format(pred_label))


predict_image(image)
print("success!")
