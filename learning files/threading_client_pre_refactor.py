import socket
import sys
import os
import io
import time
from struct import pack
import picamera
import numpy as np
import argparse


if __name__ == "__main__":
    
    # Creates Argument Parser Object to initialize host's ip
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--server-ip", required=True,
                    help="IP of Server to connect to")
    args = vars(ap.parse_args())

    host_ip = args["server_ip"]


    # Reads target port to connec tto from local text file
    with open('socket_port.txt') as f:
        line = f.readlines()

    port = int(line[0])


# init method creates a socket object
def init():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print("failed to create socket")
        sys.exit()
    print("socket created")

    check_connect(s, host_ip, port)
    

# check_connect method attempts to connect to the server using host ip on the target port,
# taking both values as parameters. if the port is open, and calls itself to repeat the
# check after 5 seconds if the port is not open
def check_connect(s, host_ip, port):
    location = (host_ip, port)
    result_of_check = s.connect_ex(location)

    if result_of_check == 0:
        print("port open")
        print("socket connected to " + str(host_ip) + " on port " + str(port))
        receive_request(s)
    else:
        print("port not open")
        time.sleep(5.0)
        check_connect(s, host_ip, port)


# receive_requests receives a user input from the server. if the received input was
# "quit", the program resets and calls init(). Otherwise, the send_images method is called.
def receive_request(s):
    msg = s.recv(1024).decode()
    if msg.lower() == "quit":
        print("quit")
        init()
    else:
        print("image requested")
        send_image(s)


# send_images function initializes the picamera and takes an image. the image is then sent to
# the server and receives a message back confirming the receipt
def send_image(s):
    print(time.time())
    with picamera.PiCamera() as camera:
        camera.shutter_speed = 3000
        camera.iso = 800
        
        camera.resolution = (640, 480)
        time.sleep(2)
        
        stream = io.BytesIO()
        
        camera.capture(stream, 'jpeg')
        print(time.time())
        camera.stop_preview()
        
    
    print("image been got")
    
    img_data = np.frombuffer(stream.getvalue(), dtype=np.uint8)
        
    length = pack('>Q', len(img_data))
    
    s.sendall(length)
    s.sendall(img_data)
    
    ds = s.recv(8)
    print(ds)
    print("thread completed")
    init()
        
init()
