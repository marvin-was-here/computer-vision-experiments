import cv2 as cv
import math
import imutils
import numpy as np
from imutils import paths


ARUCO_DICT = {
    "DICT_4X4_50": cv.aruco.DICT_4X4_50,
    "DICT_4X4_100": cv.aruco.DICT_4X4_100,
    "DICT_4X4_250": cv.aruco.DICT_4X4_250,
    "DICT_4X4_1000": cv.aruco.DICT_4X4_1000,
    "DICT_5X5_50": cv.aruco.DICT_5X5_50,
    "DICT_5X5_100": cv.aruco.DICT_5X5_100,
    "DICT_5X5_250": cv.aruco.DICT_5X5_250,
    "DICT_5X5_1000": cv.aruco.DICT_5X5_1000,
    "DICT_6X6_50": cv.aruco.DICT_6X6_50,
    "DICT_6X6_100": cv.aruco.DICT_6X6_100,
    "DICT_6X6_250": cv.aruco.DICT_6X6_250,
    "DICT_6X6_1000": cv.aruco.DICT_6X6_1000,
    "DICT_7X7_50": cv.aruco.DICT_7X7_50,
    "DICT_7X7_100": cv.aruco.DICT_7X7_100,
    "DICT_7X7_250": cv.aruco.DICT_7X7_250,
    "DICT_7X7_1000": cv.aruco.DICT_7X7_1000,
    "DICT_ARUCO_ORIGINAL": cv.aruco.DICT_ARUCO_ORIGINAL,
    "DICT_APRILTAG_16h5": cv.aruco.DICT_APRILTAG_16h5,
    "DICT_APRILTAG_25h9": cv.aruco.DICT_APRILTAG_25h9,
    "DICT_APRILTAG_36h10": cv.aruco.DICT_APRILTAG_36h10,
    "DICT_APRILTAG_36h11": cv.aruco.DICT_APRILTAG_36h11
}


def distance_to_camera(knownWidth, focalLength, perWidth):
    # compute and return distance from marker to camera
    return (knownWidth * focalLength) / perWidth

KNOWN_DISTANCE = 12.0
KNOWN_WIDTH = 11.0
focalLength = 298.94094057257064

# loads image
image = cv.imread("images/test_distance_1.png")

cv.imshow("kek", image)
cv.waitKey(0)

arucoDict = cv.aruco.Dictionary_get(ARUCO_DICT["DICT_4X4_100"])
arucoParams = cv.aruco.DetectorParameters_create()
(corners, ids, rejected) = cv.aruco.detectMarkers(image, arucoDict,
                                                        parameters=arucoParams)

if len(corners) > 0:
    ids = ids.flatten()

    # Sets coordinate values for corners
    for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners

        dx = topRight[0] - topLeft[0]
        dy = topRight[1] - topLeft[1]

        pixelCount = np.sqrt((dx ** 2) + (dy ** 2))

        #focalLength = (pixelCount * KNOWN_DISTANCE) / KNOWN_WIDTH
        #print(focalLength)

        newDistance = (focalLength * KNOWN_WIDTH) / pixelCount
        print(newDistance)

        # Each markerCorner is represented by a list of four (x, y)-coordinates (Line 70).
        # These (x, y)-coordinates represent the top-left, top-right, bottom-right,
        # and bottom-left corners of the ArUco tag (Line 71).
        # Furthermore, the (x, y)-coordinates are always returned in that order.

        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))

        # draws bounding line
        cv.line(image, topLeft, topRight, (0, 255, 0), 2)
        cv.line(image, topRight, bottomRight, (0, 255, 0), 2)
        cv.line(image, bottomRight, bottomLeft, (0, 255, 0), 2)
        cv.line(image, topLeft, bottomLeft, (0, 255, 0), 2)

        # compute and draw center (x,y) of ArUCo marker
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv.circle(image, (cX, cY), 4, (0, 0, 255), -1)

        # writes ArUCo marker ID onto image
        cv.putText(image, str(markerID),
                    (topLeft[0], topLeft[1] - 15), cv.FONT_HERSHEY_SIMPLEX,
                    0.5, (0, 255, 0), 2)
        print("[INFO] ArUCo marker ID: {}".format(markerID))

        # Displays image output
        cv.imshow("Image", image)
        cv.waitKey(0)

