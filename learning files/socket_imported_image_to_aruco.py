import socket # for sockets
import sys
import io
import struct
import numpy as np
import time
from PIL import Image
import cv2 as cv

ARUCO_DICT = {
    "DICT_4X4_50": cv.aruco.DICT_4X4_50,
    "DICT_4X4_100": cv.aruco.DICT_4X4_100,
    "DICT_4X4_250": cv.aruco.DICT_4X4_250,
    "DICT_4X4_1000": cv.aruco.DICT_4X4_1000,
    "DICT_5X5_50": cv.aruco.DICT_5X5_50,
    "DICT_5X5_100": cv.aruco.DICT_5X5_100,
    "DICT_5X5_250": cv.aruco.DICT_5X5_250,
    "DICT_5X5_1000": cv.aruco.DICT_5X5_1000,
    "DICT_6X6_50": cv.aruco.DICT_6X6_50,
    "DICT_6X6_100": cv.aruco.DICT_6X6_100,
    "DICT_6X6_250": cv.aruco.DICT_6X6_250,
    "DICT_6X6_1000": cv.aruco.DICT_6X6_1000,
    "DICT_7X7_50": cv.aruco.DICT_7X7_50,
    "DICT_7X7_100": cv.aruco.DICT_7X7_100,
    "DICT_7X7_250": cv.aruco.DICT_7X7_250,
    "DICT_7X7_1000": cv.aruco.DICT_7X7_1000,
    "DICT_ARUCO_ORIGINAL": cv.aruco.DICT_ARUCO_ORIGINAL,
    "DICT_APRILTAG_16h5": cv.aruco.DICT_APRILTAG_16h5,
    "DICT_APRILTAG_25h9": cv.aruco.DICT_APRILTAG_25h9,
    "DICT_APRILTAG_36h10": cv.aruco.DICT_APRILTAG_36h10,
    "DICT_APRILTAG_36h11": cv.aruco.DICT_APRILTAG_36h11
}

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
except socket.error:
    print("failed to create socket")
    sys.exit()
print("socket created")

s.bind(('10.19.48.109', 8000))
s.listen(1)
print("listening...")
client = s.accept()[0].makefile('rb')
print("connected")

start_time = time.time()

image_len = struct.unpack('<L', client.read(struct.calcsize('<L')))[0]
image_stream = io.BytesIO()
image_stream.write(client.read(image_len))
image_stream.seek(0)
image = Image.open(image_stream)
print('Image is %dx%d' % image.size)
client.close()
s.close()
# image.show()


image.convert('RGB')
open_cv_image = np.array(image)
open_cv_image = open_cv_image[:, :, ::-1].copy()
open_cv_image = cv.rotate(open_cv_image, cv.ROTATE_180)


arucoDict = cv.aruco.Dictionary_get(ARUCO_DICT["DICT_4X4_100"])
arucoParams = cv.aruco.DetectorParameters_create()
(corners, ids, rejected) = cv.aruco.detectMarkers(open_cv_image, arucoDict,
                                                  parameters=arucoParams)

# Check to see if at least one marker was detected
if len(corners) > 0:
    ids = ids.flatten()

    # Sets coordinate values for corners
    for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners

        # Each markerCorner is represented by a list of four (x, y)-coordinates (Line 70).
        # These (x, y)-coordinates represent the top-left, top-right, bottom-right,
        # and bottom-left corners of the ArUco tag (Line 71).
        # Furthermore, the (x, y)-coordinates are always returned in that order.

        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))

        # draws bounding line
        cv.line(open_cv_image, topLeft, topRight, (0, 255, 0), 2)
        cv.line(open_cv_image, topRight, bottomRight, (0, 255, 0), 2)
        cv.line(open_cv_image, bottomRight, bottomLeft, (0, 255, 0), 2)
        cv.line(open_cv_image, topLeft, bottomLeft, (0, 255, 0), 2)

        # compute and draw center (x,y) of ArUCo marker
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv.circle(open_cv_image, (cX, cY), 4, (0, 0, 255), -1)

        # writes ArUCo marker ID onto image
        cv.putText(open_cv_image, str(markerID),
                   (topLeft[0], topLeft[1] - 15), cv.FONT_HERSHEY_SIMPLEX,
                   0.5, (0, 255, 0), 2)
        print("[INFO] ArUCo marker ID: {}".format(markerID))

        total_runtime = time.time() - start_time
        print(total_runtime)

        cv.imshow("Marker", open_cv_image)
        cv.waitKey(0)
