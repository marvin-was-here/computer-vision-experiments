import socket
import sys
import os
from PIL import Image

import io
import time
import struct
import picamera


def init():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print("failed to create socket")
        sys.exit()
    print("socket created")

    host_ip = "10.19.48.109"
    port = 8000
    check_connect(s, host_ip, port)

def check_connect(s, host_ip, port):
    location = (host_ip, port)
    result_of_check = s.connect_ex(location)

    if result_of_check == 0:
        print("port open")
        print("socket connected to " + str(host_ip) + " on port " + str(port))
        post_connect(s)
    else:
        print("port not open")
        time.sleep(5.0)
        check_connect(s, host_ip, port)

#s.connect(location)

def post_connect(s):
    connection = s.makefile('wb')

    checker = s.recv(1024)
    print("checker received")

    while True:
        if checker:
            try:
                with picamera.PiCamera() as camera:
                    camera.resolution = (640, 480)
                    camera.start_preview()
                    time.sleep(2)
                    
                    start = time.time()
                    stream = io.BytesIO()
                    for foo in camera.capture_continuous(stream, 'jpeg'):
                        connection.write(struct.pack('<L', stream.tell()))
                        connection.flush()
                        
                        stream.seek(0)
                        connection.write(stream.read())
                        
                        if time.time() - start > 30:
                            break
                    
                    stream.seek(0)
                    stream.truncate()
                connection.write(struct.pack('<L', 0))
            finally:
                connection.close()
                s.close()
                init()


init()
