import socket # for sockets
import sys
import io
import os
import struct
import numpy as np
import time
from PIL import Image
import cv2 as cv


def init():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    except socket.error:
        print("failed to create socket")
        sys.exit()
    print("socket created")

    s.bind(('10.19.48.109', 8000))
    s.listen(1)
    print("listening...")
    client = s.accept()[0].makefile('rwb')
    print("connected")

    remainder(s, client)

def remainder(s, client):
    print("Press enter to request image")
    x = input()  # different ways of getting input perhaps

    client.write("0".encode("ascii"))
    client.flush()

    image_len = struct.unpack('<L', client.read(struct.calcsize('<L')))[0]
    image_stream = io.BytesIO()
    image_stream.write(client.read(image_len))
    image_stream.seek(0)
    image = Image.open(image_stream)
    print('Image is %dx%d' % image.size)
    client.close()
    s.close()

    process_image(image)


def process_image(pic):
    pic.convert('RGB')
    open_cv_image = np.array(pic)
    open_cv_image = open_cv_image[:, :, ::-1].copy()

    file_count = len([name for name in os.listdir('images/two_way_images/')])
    print("File Count is {}".format(file_count))

    cv.imwrite("images/two_way_images/image_{}.jpg".format(file_count), open_cv_image)

    init()


init()
