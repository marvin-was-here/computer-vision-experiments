import cv2 as cv
from imutils.perspective import four_point_transform
from imutils import contours
import imutils
import numpy as np


# Map of digits lookup
DIGITS_LOOKUP = {
	(1, 1, 1, 0, 1, 1, 1): 0,
	(0, 0, 1, 0, 0, 1, 0): 1,
	(1, 0, 1, 1, 1, 1, 0): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,
	(1, 1, 0, 1, 0, 1, 1): 5,
	(1, 1, 0, 1, 1, 1, 1): 6,
	(1, 0, 1, 0, 0, 1, 0): 7,
	(1, 1, 1, 1, 1, 1, 1): 8,
	(1, 1, 1, 1, 0, 1, 1): 9
}

#     0
#   1   2
#     3
#   4   5
#     6



def draw_general_box(image):
	COLOR = (0, 255, 0)

	cv.line(image, (160, 195), (420, 195), COLOR, 3)
	cv.line(image, (160, 260), (420, 260), COLOR, 3)

	cv.line(image, (160, 195), (160, 260), COLOR, 3)
	cv.line(image, (420, 195), (420, 260), COLOR, 3)

	return image

def draw_box(image, x1, x2, y1, y2):
	COLOR = (0, 255, 0)

	cv.line(image, (x1, y1), (x2, y1), COLOR, 3)
	cv.line(image, (x1, y2), (x2, y2), COLOR, 3)

	cv.line(image, (x1, y1), (x1, y2), COLOR, 3)
	cv.line(image, (x2, y1), (x2, y2), COLOR, 3)

	return image

def get_number(image, x1, x2, y1, y2):
	height = y2 - y1
	width = x2 - x1

	BUFFER = 5

	mid_h = int(y1 + (height/2))
	mid_w = int(x1 + (width/2))

	# check if number has top bar
	black = 0
	total = 0

	# cv.line(image, (x1 + BUFFER, y1 + BUFFER), (x2 - BUFFER, y2 - BUFFER), (0, 255, 0), 3)
	# cv.line(image, (x2 - BUFFER*2, mid_h + BUFFER), (x2 - BUFFER*2, y2 - BUFFER), (0, 255, 0), 3)


	for i in range(x1, x2):
		for j in range(y1 , y2):
	#for i in range(x1 + BUFFER, x2 - BUFFER):
	#	for j in range(y1 + BUFFER, y2 + BUFFER*2):
			# print([i, j])
			# print(image)
			if (image[i, j] == 0):
				print("black!")
				black += 1
			total += 1

	print("Black pixels: {}".format(black))
	print("Total pixels: {}".format(total))


	# cv.line(image, (x2 - BUFFER, mid_h + BUFFER), (x2 - BUFFER, y2 - BUFFER), (0, 255, 0), 3)
	# cv.line(image, (x2 - BUFFER*2, mid_h + BUFFER), (x2 - BUFFER*2, y2 - BUFFER), (0, 255, 0), 3)

	cv.imshow("img", image)
	cv.waitKey(0)

	


	

def process_image(image):
	image = imutils.resize(image, height=500)
	gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
	blurred = cv.GaussianBlur(gray, (5, 5), 0)
	edged = cv.Canny(blurred, 50, 200, 255)

	cv.imshow("edged", edged)
	cv.waitKey(0)

	cnts = cv.findContours(edged.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	cnts = sorted(cnts, key=cv.contourArea, reverse=True)[2:5]
	displayCnt = None

	for c in cnts:
		peri = cv.arcLength(c, True)
		approx= cv.approxPolyDP(c, 0.02 * peri, True)


		if len(approx) == 4:
			displayCnt = approx
			break
	


	warped = four_point_transform(gray, displayCnt.reshape(4, 2))
	output = four_point_transform(image, displayCnt.reshape(4, 2))

	# cv.drawContours(output, [displayCnt], -1, (0, 255, 0), 2)

	warped = imutils.resize(warped, height=700)
	output = imutils.resize(output, height=700)

	# morpho
	thresh = cv.threshold(warped, 0, 255,
		cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
	kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (1, 5))
	thresh = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel)

	# draw_general_box(thresh)
	
	# thresh = draw_box(thresh, 285, 335, 195, 263)
	# thresh = draw_box(thresh, 345, 395, 195, 263)

	get_number(thresh, 285, 335, 195, 263)
	# get_number(thresh, 345, 395, 195, 263)

	# cv.imshow("image", thresh)
	# cv.waitKey(0)

image = cv.imread("images/test_timer_cap_5.jpg")
process_image(image)