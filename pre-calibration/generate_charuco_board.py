import time
import cv2.aruco as A
import cv2 as cv
import numpy as np

dictionary = A.getPredefinedDictionary(A.DICT_4X4_1000)

SQUARE_SIZE = 25.4
MARKER_SIZE = 0.75 * SQUARE_SIZE

board = A.CharucoBoard_create(13, 9, SQUARE_SIZE, MARKER_SIZE, dictionary)  # 13 in by 9 in
image = board.draw((200*13, 200*9))  # tuple for pixel size

cv.imwrite("tags/ChArUCo Board.png", image)
cv.imshow("ChArUCo Board", image)
cv.waitKey(0)
