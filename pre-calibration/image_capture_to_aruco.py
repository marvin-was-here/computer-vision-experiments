import cv2 as cv

# Detects and marks ArUCo markers from images captured on webcam (no script required!)

# Creates video capture object
cam = cv.VideoCapture(0)

cv.namedWindow("Placeholder")
 
# Initializes key value pairs of possible ArUCo dictionaries
ARUCO_DICT = {
    "DICT_4X4_50": cv.aruco.DICT_4X4_50,
    "DICT_4X4_100": cv.aruco.DICT_4X4_100,
    "DICT_4X4_250": cv.aruco.DICT_4X4_250,
    "DICT_4X4_1000": cv.aruco.DICT_4X4_1000,
    "DICT_5X5_50": cv.aruco.DICT_5X5_50,
    "DICT_5X5_100": cv.aruco.DICT_5X5_100,
    "DICT_5X5_250": cv.aruco.DICT_5X5_250,
    "DICT_5X5_1000": cv.aruco.DICT_5X5_1000,
    "DICT_6X6_50": cv.aruco.DICT_6X6_50,
    "DICT_6X6_100": cv.aruco.DICT_6X6_100,
    "DICT_6X6_250": cv.aruco.DICT_6X6_250,
    "DICT_6X6_1000": cv.aruco.DICT_6X6_1000,
    "DICT_7X7_50": cv.aruco.DICT_7X7_50,
    "DICT_7X7_100": cv.aruco.DICT_7X7_100,
    "DICT_7X7_250": cv.aruco.DICT_7X7_250,
    "DICT_7X7_1000": cv.aruco.DICT_7X7_1000,
    "DICT_ARUCO_ORIGINAL": cv.aruco.DICT_ARUCO_ORIGINAL,
    "DICT_APRILTAG_16h5": cv.aruco.DICT_APRILTAG_16h5,
    "DICT_APRILTAG_25h9": cv.aruco.DICT_APRILTAG_25h9,
    "DICT_APRILTAG_36h10": cv.aruco.DICT_APRILTAG_36h10,
    "DICT_APRILTAG_36h11": cv.aruco.DICT_APRILTAG_36h11
}

while True:
    ret, frame = cam.read()  # Reads video stream
    if not ret:  # Checks to see if video stream is read
        print("Failed to grab frame")
        break

    # Displays video stream in new window
    cv.imshow("Video Stream", frame)

    # Checks to see if keys are pressed
    k = cv.waitKey(1)
    if k % 256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k % 256 == 32:
        # Space key pressed
        img_name = "capture_tests/capture_test_01.png"
        image = frame

        # Sets aruco dictionary, parameters
        # Detects markers in image
        arucoDict = cv.aruco.Dictionary_get(ARUCO_DICT["DICT_4X4_100"])
        arucoParams = cv.aruco.DetectorParameters_create()
        (corners, ids, rejected) = cv.aruco.detectMarkers(image, arucoDict,
                                                          parameters=arucoParams)

        # Check to see if at least one marker was detected
        if len(corners) > 0:
            ids = ids.flatten()

            # Sets coordinate values for corners
            for (markerCorner, markerID) in zip(corners, ids):
                corners = markerCorner.reshape((4, 2))
                (topLeft, topRight, bottomRight, bottomLeft) = corners

                # Each markerCorner is represented by a list of four (x, y)-coordinates (Line 70).
                # These (x, y)-coordinates represent the top-left, top-right, bottom-right,
                # and bottom-left corners of the ArUco tag (Line 71).
                # Furthermore, the (x, y)-coordinates are always returned in that order.

                topRight = (int(topRight[0]), int(topRight[1]))
                bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
                bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
                topLeft = (int(topLeft[0]), int(topLeft[1]))

                # draws bounding line
                cv.line(image, topLeft, topRight, (0, 255, 0), 2)
                cv.line(image, topRight, bottomRight, (0, 255, 0), 2)
                cv.line(image, bottomRight, bottomLeft, (0, 255, 0), 2)
                cv.line(image, topLeft, bottomLeft, (0, 255, 0), 2)

                # compute and draw center (x,y) of ArUCo marker
                cX = int((topLeft[0] + bottomRight[0]) / 2.0)
                cY = int((topLeft[1] + bottomRight[1]) / 2.0)
                cv.circle(image, (cX, cY), 4, (0, 0, 255), -1)

                # writes ArUCo marker ID onto image
                cv.putText(image, str(markerID),
                           (topLeft[0], topLeft[1] - 15), cv.FONT_HERSHEY_SIMPLEX,
                           0.5, (0, 255, 0), 2)
                print("[INFO] ArUCo marker ID: {}".format(markerID))

                # Displays image output
                cv.imshow("Image", image)
                cv.waitKey(0)
