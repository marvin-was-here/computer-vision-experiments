import cv2 as cv

# Press space to take a capture, up to 10
# Press ESC to close out windows

# Loads video capture
cam = cv.VideoCapture(0)

cv.namedWindow("Image Window")

img_counter = 0

while True:
    ret, frame = cam.read()

    # checks to see if frames load
    if not ret:
        print("failed to get frame")
        break

    # shows window with video stream
    cv.imshow("Video Stream", frame)

    # Waits for user input
    k = cv.waitKey(1)

    # Performs one of two actions based on key pressed
    if k%256 == 27:
        # ESC key pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # space key pressed
        img_name = "images/opencv_frame_{}.png".format(img_counter)
        cv.imwrite(img_name, frame)  # saves image
        print("image written")
        img_counter += 1

cam.release()

cv.destroyAllWindows()


