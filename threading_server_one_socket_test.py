import threading
from threading_class_import import NewSocket
import sys

# server_ip = 10.19.48.109 # apl macbook
# server_ip = "192.168.0.13" # home macbook
server_ip = "192.168.0.21" # home windows

t1 = NewSocket(1, server_ip, 8001)

t1.run()

while True:
    print("[INPUT] Type \"image\" to request an image, type \"quit\" to exit")
    x = input()

    if x == "quit":
        print("[STATUS] Stopping Program")
        sys.exit()
    else:
        t1.request_image()
