import cv2 as cv

PATH = "calibration_charuco.yaml"

cv_file = cv.FileStorage(PATH, cv.FILE_STORAGE_READ)

camera_matrix = cv_file.getNode('K').mat()
dist_matrix = cv_file.getNode('D').mat()

cv_file.release()

# print(camera_matrix)
# print(dist_matrix)

original = cv.imread("images/calibration_images/charuco_images/charuco_calib5.png")
dst = cv.undistort(original, camera_matrix, dist_matrix, None, None)

cv.imshow("original", original)
cv.imshow("undistorted", dst)
cv.waitKey(0)

