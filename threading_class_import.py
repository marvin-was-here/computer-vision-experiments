import socket
import sys
import threading
import os
from struct import unpack
import numpy as np
import cv2 as cv
import time

from aruco_class_import import ArucoDetector


class NewSocket (threading.Thread):

    def __init__(self, thread_id, host_ip, port):
        threading.Thread.__init__(self)
        self.id = thread_id
        self.ip = host_ip
        self.port = port
        self.AD = ArucoDetector()

    def run(self):
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except socket.error:
            print("[ERROR] Failed to create socket")
            sys.exit()
        print("[STATUS] Socket created for port {}".format(self.port))

        self.s.bind((self.ip, self.port))
        self.s.listen(1)
        print("[STATUS] Listening for connections...")
        self.client = self.s.accept()[0]
        print("[STATUS] Connected to {} on port {}".format(self.ip, self.port))
        # self.request_image(s, self.client)

    def request_image(self):
        print("[STATUS] Sending request to client")
        self.client.send("image".encode())
        print("[STATUS] Request sent to client")
        self.receive_image()
        # self.receive_image(s, client)

    def receive_image(self):
        byte_stream = self.client.recv(8)
        (length,) = unpack('>Q', byte_stream)
        data = b''
        while len(data) < length:
            to_read = length - len(data)
            data += self.client.recv(
                4096 if to_read > 4096 else to_read)

        print("[STATUS] Image received")

        open_cv_image = cv.imdecode(np.frombuffer(data, np.uint8), 1)
        # open_cv_image = cv.rotate(open_cv_image, cv.ROTATE_180)
        self.process_image(open_cv_image)
        # process_image(open_cv_image, self.client)

    def process_image(self, image):
        self.AD.load_dictionary("DICT_4x4_50")
        processed_image = self.AD.detect_markers(image)
        self.save_image(processed_image)


    def save_image(self, open_cv_image):

        # file_count = len([name for name in os.listdir('images/four_way_images/')])
        # print("File Count is {}".format(file_count))
        cv.imwrite("images/detection_test_images/image_{}.jpg".format(self.port - 7999), open_cv_image)

        # self.s.close()

        data2 = "received"
        self.client.send(data2.encode())

        # self.client.close()

        # self.run()
