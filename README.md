# Underwater Motion Capture System
Marvin Lin, 2021 :))

Because the Qualisys System costs a lot of money


# Packages Required
- Python 3.9
    - numpy
    - imutils
    - opencv-python
    - opencv-contrib-python


# List of Marvin's Pi's at the Lab : 

HostName : pi-camera1           Target Port : 8000           IP : 10.18.178.62  
HostName : pi-camera2           Target Port : 8001           IP : 10.19.122.69  
HostName : pi-camera3           Target Port : 8002           IP : 10.18.192.215  
HostName : pi-camera4           Target Port : 8003           IP : 10.19.94.236  
HostName : pi-camera5           Target Port : 8004           IP : 10.19.22.23  



# Rudimentary Instructions

How to use the code (thus far):

    - threading_server.py runs on the desired server machine. this machine will:
        - simultaneously send out the commands to whichever raspberry pi devices are connected to it
        - receive and process all images
        - import the struture of a Socket object from threading_class_import.py

    - threading_client.py will need to run on each individual Raspberry Pi (with a camera module attached and enabled)
        - a socket_port.txt file containing the port number that corresponds to the Raspberry Pi
    
    1. run threading_server.py on the server
    2. run ```python threading_client.py -s <server ip>``` on each client pi
    3. if all scripts are running successfully (the order they are run in does not matter), sockets for each raspberry pi should be created and
        connected to, after which the user should be prompted to enter an input on the server device. If a valid command is entered, a request 
        for an image will be sent to each pi, which takes an image and sends it back over the TCP socket to the server for processing.  
