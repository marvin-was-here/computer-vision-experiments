import socket
import sys
import os
import io
import time
from struct import pack
import picamera
import numpy as np
import argparse



# class PiImageSender contains functions to create an image sender object
class PiImageSender():
    def __init__(self):
        pass
    
    
    # init method creates a socket object
    def create_socket(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            print("[STATUS] Failed to create socket")
            sys.exit()
        print("[STATUS] Socket created")
        
        self.camera = picamera.PiCamera()
        self.camera.shutter_speed = 3000
        self.camera.iso = 800
        self.camera.resolution = (640, 480)
        time.sleep(2)
            

        self.check_connect(s, host_ip, port)
    
    
    # check_connect method attempts to connect to the server using host ip on the target port,
    # taking both values as parameters. if the port is open, and calls itself to repeat the
    # check after 5 seconds if the port is not open
    def check_connect(self, s, host_ip, port):
        location = (host_ip, port)
        result_of_check = s.connect_ex(location)

        if result_of_check == 0:
            print("[STATUS] Socket has been connected to " + str(host_ip) + " on port " + str(port))
            self.receive_request(s)
        else:
            print("[STATUS] Port " + str(port) + " is not open, trying again in 3 seconds")
            time.sleep(3.0)
            self.check_connect(s, host_ip, port)
            
            
    # receive_requests receives a user input from the server. if the received input was
    # "quit", the program resets and calls init(). Otherwise, the send_images method is called.
    def receive_request(self, s):
        print("[STATUS] Waiting for server input")
        msg = s.recv(1024).decode()
        if msg:
            if msg.lower() == "quit":
                print("[STATUS] Quitting session")
                self.create_socket()
            else:
                print("[STATUS] Image requested")
                self.send_image(s)
        else:
            print("[STATUS] Socket Closed")
            
            
    # send_images function initializes the picamera and takes an image. the image is then sent to
    # the server and receives a message back confirming the receipt
    def send_image(self, s):

        stream = io.BytesIO()
        
        self.camera.capture(stream, 'jpeg')
            
        
        print("[STATUS] Image taken")
        
        img_data = np.frombuffer(stream.getvalue(), dtype=np.uint8)
            
        length = pack('>Q', len(img_data))
        
        s.sendall(length)
        s.sendall(img_data)
        
        print("[STATUS] Image sent")
        
        ds = s.recv(1024).decode()
        print("[STATUS] Image received, restarting loop")
        
        time.sleep(1.0)
        self.receive_request(s)       
            

if __name__ == "__main__":
    
    # Creates Argument Parser Object to initialize host's ip
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--server-ip", required=True,
                    help="IP of Server to connect to")
    args = vars(ap.parse_args())

    host_ip = args["server_ip"]


    # Reads target port to connec tto from local text file
    with open('socket_port.txt') as f:
        line = f.readlines()

    port = int(line[0])
    
    socket_object = PiImageSender()
    socket_object.create_socket()
        

