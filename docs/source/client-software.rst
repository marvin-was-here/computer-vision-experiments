Client Software
========================

*The following instructions will teach you how to set up the software aspect of the client devices for this project*

.. note :: These steps should be done **after** the client hardware has been set up:


The software for the clients takes requests from the server and captures, compresses, and sends images back.

Python3
++++++++++

The Raspberry Pi Raspbian software should come with Python3 installed and enabled.


Creating a Virtual Environment
+++++++++++++++++++++++++++++++++

Creation of a virtual environment for the project is recommended because it makes the code and packages easier to organize. Create a virtual environment by doing the following (taken from `this link <https://geektechstuff.com/2019/01/14/creating-a-virtual-python-environment-python/>`_):

#. Open a terminal
#. Create a folder for the project using ``mkdir <folder name>``
#. Navigate to the project folder using ``cd <folder name>``
#. Create the virtual environment using ``python3 -m venv./venv``

This should create a folder named "venv" in the project folder. The virtual environment can be activated by running ``source ./venv/bin/activate`` or maybe ``workon <virtual environment name>``. All required packages should be installed in the virtual environment directory.

..
	fix this activation line


Installing Packages
+++++++++++++++++++++++

Like with the server, packages can be installed with pip using the command ``pip install <package name>``. Note that before running this process in the terminal, you must cd into the root directory of the imported project code to access the packages.

The required package names are listed below:
* 